package ascend;

import java.awt.event.KeyEvent;
import java.util.concurrent.CopyOnWriteArrayList;

import org.lwjgl.opengl.Display;

import ascend.events.Event;
import ascend.modules.Module;
import ascend.modules.movement.Fly;
import ascend.modules.movement.Sprint;
import ascend.ui.HUD;

public class Client {

	public static String name = "Aleph-0", version = "0.0.1";
	public static CopyOnWriteArrayList<Module> modules = new CopyOnWriteArrayList<Module>();
	public static HUD hud = new HUD();
	// This run on startup
	public static void startup() {
		System.out.print("Starting " + name + " - v" + version);
		Display.setTitle(name + " v" + version);
	    // Register modules
		modules.add(new Fly());
		modules.add(new Sprint());
	}
	
	public static void onEvent(Event e) {
		for (Module m : modules) {
			if(!m.toggled)
				continue;
			
			m.onEvent(e);
		}
	}
	
	public static void keyPress(int key) {
		for(Module m : modules) {
			if(m.getKey() == key) {
				m.toggle();
			}
		}
	}
	
}
