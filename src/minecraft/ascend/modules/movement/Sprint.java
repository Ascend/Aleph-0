package ascend.modules.movement;

import org.lwjgl.input.Keyboard;

import ascend.events.Event;
import ascend.events.listeners.EventUpdate;
import ascend.modules.Module;

public class Sprint extends Module{

	public Sprint() {
		super("Sprint", Keyboard.KEY_R, Category.MOVEMENT);
	}
	
	public void onEnable() {
	}
	
	public void onDisable() {		
		mc.thePlayer.setSprinting(false);
	}
	
	public void onEvent(Event e) {
		if(e instanceof EventUpdate) {
			if(e.isPre()) {
				mc.thePlayer.setSprinting(true);
			}
		}
	}
	
}
