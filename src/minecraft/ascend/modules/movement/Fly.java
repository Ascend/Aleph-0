package ascend.modules.movement;

import org.lwjgl.input.Keyboard;

import ascend.events.Event;
import ascend.events.listeners.EventUpdate;
import ascend.modules.Module;

public class Fly extends Module{

	public Fly() {
		super("Fly", Keyboard.KEY_N, Category.MOVEMENT);
	}
	
	public void onEnable() {
		mc.thePlayer.capabilities.isFlying = true;
		mc.thePlayer.capabilities.allowFlying = true;
	}
	
	public void onDisable() {
		mc.thePlayer.capabilities.isFlying = false;
		mc.thePlayer.capabilities.allowFlying = false;		
	}
	
	public void onEvent(Event e) {
		if(e instanceof EventUpdate) {
			if(e.isPre()) {
				
			}
		}
	}
	
}
